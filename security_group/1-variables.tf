variable "vpc_id" {
  type = string
}

variable "env" {
  type = string
}

variable "ec2_sg_name" {
  type = string
}

variable "ec2_jenkins_sg_name" {
  type = string
}
