variable "lb-TG-name" {}
variable "lg-TG-port" {}
variable "lg-TG-protocol" {}
variable "vpc_id" {}
variable "ec2_instance_id" {}




resource "aws_lb_target_group" "this" {
  name     = var.lb-TG-name
  port     = var.lg-TG-port
  protocol = var.lg-TG-protocol
  vpc_id   = var.vpc_id

  health_check {
    path                = "/login"
    port                = 8080
    healthy_threshold   = 6
    unhealthy_threshold = 2
    timeout             = 2
    interval            = 5
    matcher             = "200" # has to be HTTP 200 or fails
  }
}



resource "aws_lb_target_group_attachment" "this" {
  target_group_arn = aws_lb_target_group.this.arn
  target_id        = var.ec2_instance_id
  port             = 8080
}







output "aws_lb_TG_arn" {
  value = aws_lb_target_group.this.arn
}







