resource "aws_vpc" "this" {
  cidr_block = var.vpc_cidr_block

  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "${var.env}-main"
  }
}


output "aws_vpc_id" {
  value = aws_vpc.this.id
}
