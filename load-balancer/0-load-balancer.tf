variable "lb_name" {}
variable "is_internal" { default = false }
variable "lb_type" {}
variable "SG_port_20_80" {}
variable "subnet_ids" {}
variable "env" {}
variable "lb_TG_arn" {}
variable "ec2_instance_id" {}
variable "lb_TG_attachment_port" {}


variable "lb_listner_port" {}
variable "lb_listner_protocol" {}
variable "lb_listner_default_action" {}
variable "lb_https_listner_port" {}
variable "lb_https_listner_protocol" {}









resource "aws_lb" "this" {
  name               = var.lb_name
  internal           = var.is_internal
  load_balancer_type = var.lb_type
  security_groups    = [var.SG_port_20_80]
  subnets            = var.subnet_ids

  enable_deletion_protection = false

  tags = {
    Name = "${var.env}-terraform-lb"
  }

}


resource "aws_lb_target_group_attachment" "this" {
  target_group_arn = var.lb_TG_arn
  target_id        = var.ec2_instance_id
  port             = var.lb_TG_attachment_port

}


resource "aws_lb_listener" "dev_proj_1_lb_listner" {
  load_balancer_arn = aws_lb.this.arn
  port              = var.lb_listner_port
  protocol          = var.lb_listner_protocol

  default_action {
    type             = var.lb_listner_default_action
    target_group_arn = var.lb_TG_arn
  }
}
