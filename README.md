# AWS Terraform Gitlab CICD



## Architecture of the project
![Untitled](/images/infra-1.png)
![Untitled](/images/infra-2.png)



Terraform is a tool for building, changing, and versioning infrastructure safely and efficiently. Terraform can manage existing and popular service providers as well as custom in-house solutions.

The key features of Terraform are:

- **Infrastructure as Code**: Infrastructure is described using a high-level configuration syntax. This allows a blueprint of your datacenter to be versioned and treated as you would any other code. Additionally, infrastructure can be shared and re-used.
- **Execution Plans**: Terraform has a "planning" step where it generates an execution plan. The execution plan shows what Terraform will do when you call apply. This lets you avoid any surprises when Terraform manipulates infrastructure.
- **Change Automation**: Complex changesets can be applied to your infrastructure with minimal human interaction. With the previously mentioned execution plan and resource graph, you know exactly what Terraform will change and in what order, avoiding many possible human errors.

## Prerequisites

1. [AWS Account](https://aws.amazon.com/account/)
2. [Install AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
3. [Install Terraform](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli) 
4. [Configure AWS credentials](https://docs.aws.amazon.com/cli/v1/userguide/cli-chap-configure.html)
5. [GitLab Account](https://gitlab.com)


### To clone the project
```bash
git clone https://gitlab.com/surajb1515/aws-terraform-gitlab-cicd.git
```


### To test the application in local machine

```bash
terraform init
```

![Untitled](/images/terraform-init.png)


---
```bash
terraform validate
```

![Untitled](/images/terraform-validate.png)


---
```bash
terraform plan
```
![Untitled](/images/terraform-plan.png)



---
```bash
terraform apply --auto-approve
```
![Untitled](/images/terraform-apply.png)


---
```bash
terraform destroy --auto-approve
```
![Untitled](/images/terraform-destory.png)





## Creating CI/CD pipeline using GitLab CICD

Now, Create .gitlab-ci.yml, it will trigger the pipeline

This CI/CD pipeline consists of the following stages

1. validate stage
2. plan stage
3. apply stage
4. destroy stage


![Untitled](/images/cicd.png)


