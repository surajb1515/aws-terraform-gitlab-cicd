variable "instance_type" {}
variable "ami_id" {}
variable "subnet_id" {}
variable "enable_public_ip_address" {}
variable "jenkins_sg_id" {}
variable "user_data_install_jenkins" {}
variable "env" {}


# jenkins-ec2-instance on public subnet

resource "aws_instance" "this" {
  ami                         = var.ami_id
  instance_type               = var.instance_type
  key_name                    = "ap-south-1-key"
  subnet_id                   = var.subnet_id
  vpc_security_group_ids      = var.jenkins_sg_id
  associate_public_ip_address = var.enable_public_ip_address

  user_data = var.user_data_install_jenkins

  tags = {
    Name = "${var.env}-main"
  }
}



output "aws_ec2_jenkins_id" {
  value = aws_instance.this.id
}
