module "vpc" {
  source               = "./vpc"
  vpc_cidr_block       = "10.0.0.0/16"
  env                  = "dev"
  public_subnets_cidr  = ["10.0.1.0/24", "10.0.2.0/24"]
  private_subnets_cidr = ["10.0.3.0/24"]
  azs                  = ["ap-south-1a", "ap-south-1b"]

  private_subnet_tags = {
    "suraj" = "owner"
  }
  public_subnet_tags = {
    "suraj" = "owner"
  }
}


module "SG_module" {
  source              = "./security_group"
  env                 = "dev"
  ec2_sg_name         = "dev-22_80_443_sg"
  vpc_id              = module.vpc.aws_vpc_id
  ec2_jenkins_sg_name = "jenkins_8080_sg"

}



module "jenkins" {
  source                    = "./jenkins-on-ec2"
  instance_type             = "t2.micro"
  ami_id                    = "ami-0f58b397bc5c1f2e8"
  subnet_id                 = tolist(module.vpc.aws_subnet_public_ids)[0]
  enable_public_ip_address  = true
  jenkins_sg_id             = [module.SG_module.SG_id_ec2_SG_port_22_80, module.SG_module.SG_id_SG_port_8080]
  user_data_install_jenkins = templatefile("./jenkins-runner-script/jenkins-installer.sh", {})
  env                       = "dev"
}




module "lb-TG" {
  source          = "./load-balancer-TG"
  lb-TG-name      = "jenkins-lb-TG"
  lg-TG-port      = 8080
  lg-TG-protocol  = "HTTP"
  vpc_id          = module.vpc.vpc_id
  ec2_instance_id = module.jenkins.aws_ec2_jenkins_id
}




module "alb" {
  source        = "./load-balancer"
  lb_name       = "dev-project-alb"
  is_internal   = false
  lb_type       = "application"
  SG_port_20_80 = module.SG_module.SG_id_ec2_SG_port_22_80
  subnet_ids    = tolist(module.vpc.aws_subnet_public_ids)
  env           = "dev"

  lb_TG_arn       = module.lb-TG.aws_lb_TG_arn
  ec2_instance_id = module.jenkins.aws_ec2_jenkins_id


  lb_listner_port           = 80
  lb_listner_protocol       = "HTTP"
  lb_listner_default_action = "forward"

  lb_https_listner_port     = 443
  lb_https_listner_protocol = "HTTPS"
  lb_TG_attachment_port     = 8080

}
